# Donaswap V3


## Deployments

1. Add Keys in `.env` file. It's a private key of the account that will deploy the contracts and should be gitignored.
2. Private Key of deployer `DEPLOYER_KEY`.
3. `EXPLORER_API_KEY` in `.env` file. It's an API key for block explorers.
4. `yarn` in root directory
5. `NETWORK=$NETWORK yarn zx v3-deploy.mjs` where `$NETWORK` is either 
`eth`,
`goerli`,
`sepolia`,
`holesky`,
`bscMainnet`,
`bscTestnet`,
`opBnb`,
`opBnbTestnet`,
`polygon`,
`polygonMumbai`,
`polygonZkevm`,
`polygonZkevmTestnet`,
`avalanche`,
`avalancheFuji`,
`fantom`,
`fantomTestnet`,
`arbitrum`,
`arbitrumGoerli`,
`optimism`,
`optimismGoerli`,
`linea`,
`lineaGoerli`, or
`hardhat` (for local testing)
6. `NETWORK=$NETWORK yarn zx v3-verify.mjs` where `$NETWORK` is either 
`eth`,
`goerli`,
`sepolia`,
`holesky`,
`bscMainnet`,
`bscTestnet`,
`opBnb`,
`opBnbTestnet`,
`polygon`,
`polygonMumbai`,
`polygonZkevm`,
`polygonZkevmTestnet`,
`avalanche`,
`avalancheFuji`,
`fantom`,
`fantomTestnet`,
`arbitrum`,
`arbitrumGoerli`,
`optimism`,
`optimismGoerli`,
`linea`,
`lineaGoerli`,
`palm`,
`palmTestnet`,
`celo`,
`celoAlfajores`,
`base`,
`baseGoerli`,
`shibarium`,
`shibariumPuppynet`,
`fusion`,
`fusionTestnet`,
`cronos`,
`cronosTestnet`, or
`hardhat` (for local testing)