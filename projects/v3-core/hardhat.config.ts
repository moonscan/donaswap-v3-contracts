import '@nomiclabs/hardhat-ethers'
import '@nomiclabs/hardhat-etherscan'
import '@nomiclabs/hardhat-waffle'
import '@typechain/hardhat'
import 'hardhat-watcher'
import 'dotenv/config'
import 'solidity-docgen'
require('dotenv').config({ path: require('find-config')('.env') })

const LOWEST_OPTIMIZER_COMPILER_SETTINGS = {
  version: '0.7.6',
  settings: {
    evmVersion: 'istanbul',
    optimizer: {
      enabled: true,
      runs: 400,
    },
    metadata: {
      bytecodeHash: 'none',
    },
  },
}

const DEFAULT_COMPILER_SETTINGS = {
  version: '0.7.6',
  settings: {
    evmVersion: 'istanbul',
    optimizer: {
      enabled: true,
      runs: 1_000_000,
    },
    metadata: {
      bytecodeHash: 'none',
    },
  },
}

export default {
  networks: {
    hardhat: {
      allowUnlimitedContractSize: true,
    },
    firechain: {
      url: 'https://rpc.thefirechain.com',
      accounts: [process.env.DEPLOYER_KEY!],
    },
    rinia: {
      url: 'https://rpc1.rinia.thefirechain.com',
      accounts: [process.env.DEPLOYER_KEY!],
    },
    ethereum: {
      url: `https://mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    goerli: {
      url: `https://goerli.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    sepolia: {
      url: `https://sepolia.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    holesky: {
      url: `https://ethereum-holesky.publicnode.com`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    bscMainnet: {
      url: 'https://bsc-dataseed.binance.org/',
      accounts: [process.env.DEPLOYER_KEY!],
    },
    bscTestnet: {
      url: 'https://data-seed-prebsc-1-s1.binance.org:8545/',
      accounts: [process.env.DEPLOYER_KEY!],
    },
    opBnb: {
      url: "https://opbnb-mainnet-rpc.bnbchain.org",
      accounts: [process.env.DEPLOYER_KEY!],
    },
    opBnbTestnet: {
      url: "https://opbnb-testnet-rpc.bnbchain.org",
      accounts: [process.env.DEPLOYER_KEY!],
    },
    polygon: {
      url: `https://polygon-mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    polygonMumbai: {
      url: `https://polygon-mumbai.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    polygonZkevm: {
      url: `https://zkevm-rpc.com`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    polygonZkevmTestnet: {
      url: `https://rpc.public.zkevm-test.net`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    avalanche: {
      url: `https://avalanche-mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    avalancheFuji: {
      url: `https://avalanche-fuji.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    fantom: {
      url: `https://rpc.ftm.tools`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    fantomTestnet: {
      url: `https://rpc.testnet.fantom.network`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    arbitrum: {
      url: `https://arbitrum-mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    arbitrumGoerli: {
      url: `https://arbitrum-goerli.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    optimism: {
      url: `https://optimism-mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    optimismGoerli: {
      url: `https://optimism-goerli.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    linea: {
      url: `https://linea-mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    lineaGoerli: {
      url: `https://linea-goerli.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    palm: {
      url: `https://palm-mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    palmTestnet: {
      url: `https://palm-testnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    celo: {
      url: `https://celo-mainnet.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    celoAlfajores: {
      url: `https://celo-alfajores.infura.io/v3/${process.env.INFURA_API_KEY}`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    base: {
      url: 'https://mainnet.base.org',
      accounts: [process.env.DEPLOYER_KEY!],
      gasPrice: 1000000000,
    },
    baseGoerli: {
      url: 'https://goerli.base.org',
      accounts: [process.env.DEPLOYER_KEY!],
      gasPrice: 1000000000,
    },
    shibarium: {
      url: 'https://www.shibrpc.com',
      accounts: [process.env.DEPLOYER_KEY!],
    },
    shibariumPuppynet: {
      url: 'https://puppynet.shibrpc.com',
      accounts: [process.env.DEPLOYER_KEY!],
    },
    fusion: {
      url: `https://mainnet.fusionnetwork.io`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    fusionTestnet: {
      url: `https://testnet.fusionnetwork.io`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    cronos: {
      url: `https://evm.cronos.org`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
    cronosTestnet: {
      url: `https://evm-t3.cronos.org/`,
      accounts: [process.env.DEPLOYER_KEY!],
    },
  },
  etherscan: {
    apiKey: process.env.EXPLORER_API_KEY,

  },
  customChains: [
    {
      network: "opBnbTestnet",
      chainId: 5611,
      urls: {
        apiURL: "https://api-opbnb-testnet.bscscan.com/api",
        browserURL: "https://opbnb-testnet.bscscan.com/"
      }
    },
    {
      network: "optimismGoerli",
      chainId: 420,
      urls: {
        apiURL: "https://api-goerli-optimistic.etherscan.io/",
        browserURL: "https://optimistic.etherscan.io/"
      }
    },
    {
      network: "linea",
      chainId: 59144,
      urls: {
        apiURL: "https://api.lineascan.build/api",
        browserURL: "https://lineascan.build/"
      }
    },
    {
      network: "lineaGoerli",
      chainId: 59140,
      urls: {
        apiURL: "https://api-testnet.lineascan.build/api",
        browserURL: "https://goerli.lineascan.build/"
      }
    },
    {
      network: "polygonZkevm",
      chainId: 1101,
      urls: {
        apiURL: "https://api-zkevm.polygonscan.com/api",
        browserURL: "https://zkevm.polygonscan.com"
      }
    },
    {
      network: "polygonZkevmTestnet",
      chainId: 1442,
      urls: {
        apiURL: "https://api-testnet.lineascan.build/api",
        browserURL: "https://api-testnet-zkevm.polygonscan.com/api"
      }
    },
    {
      network: "base",
      chainId: 8453,
      urls: {
        apiURL: "https://api-zkevm.polygonscan.com/api",
        browserURL: "https://basescan.org"
      }
    },
    {
      network: "baseGoerli",
      chainId: 84531,
      urls: {
        apiURL: "https://api-testnet.lineascan.build/api",
        browserURL: "https://goerli.basescan.org/"
      }
    },
    // {
    //   network: "shibarium",
    //   chainId: 109,
    //   urls: {
    //     apiURL: "https://api-zkevm.polygonscan.com/api",
    //     browserURL: "https://www.shibariumscan.io"
    //   }
    // },
    // {
    //   network: "shibariumPuppynet",
    //   chainId: 719,
    //   urls: {
    //     apiURL: "",
    //     browserURL: "https://puppyscan.shib.io"
    //   }
    // },
    // {
    //   network: "holesky",
    //   chainId: 17000,
    //   urls: {
    //     apiURL: "https://api-opbnb-testnet.bscscan.com/api",
    //     browserURL: "https://opbnb-testnet.bscscan.com/"
    //   }
    // },
  ],
  solidity: {
    compilers: [DEFAULT_COMPILER_SETTINGS],
    overrides: {
      'contracts/DonaswapV3Pool.sol': LOWEST_OPTIMIZER_COMPILER_SETTINGS,
      'contracts/DonaswapV3PoolDeployer.sol': LOWEST_OPTIMIZER_COMPILER_SETTINGS,
      'contracts/test/OutputCodeHash.sol': LOWEST_OPTIMIZER_COMPILER_SETTINGS,
    },
  },
  watcher: {
    test: {
      tasks: [{ command: 'test', params: { testFiles: ['{path}'] } }],
      files: ['./test/**/*'],
      verbose: true,
    },
  },
  docgen: {
    pages: 'files',
  },
}
