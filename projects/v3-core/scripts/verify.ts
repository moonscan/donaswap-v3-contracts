import { verifyContract } from '@donaswap/common/verify'
import { sleep } from '@donaswap/common/sleep'
import { network } from "hardhat";

async function main() {
  const networkName = network.name
  const deployedContracts = await import(`@donaswap/v3-core/deployments/${networkName}.json`)

  // Verify DonaswapV3PoolDeployer
  console.log('Verify DonaswapV3PoolDeployer')
  await verifyContract(deployedContracts.DonaswapV3PoolDeployer)
  await sleep(10000)

  // Verify donaswapV3Factory
  console.log('Verify donaswapV3Factory')
  await verifyContract(deployedContracts.DonaswapV3Factory, [deployedContracts.DonaswapV3PoolDeployer])
  await sleep(10000)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
