import { ethers, network } from 'hardhat'
import { configs } from '@donaswap/common/config'
import fs from 'fs'
import { abi } from '@donaswap/v3-core/artifacts/contracts/DonaswapV3Factory.sol/DonaswapV3Factory.json'

async function main() {
  const [owner] = await ethers.getSigners()
  // Remember to update the init code hash in SC for different chains before deploying
  const networkName = network.name
  const config = configs[networkName as keyof typeof configs]
  if (!config) {
    throw new Error(`No config found for network ${networkName}`)
  }

  const v3DeployedContracts = await import(`@donaswap/v3-core/deployments/${networkName}.json`)
  const mcV3DeployedContracts = await import(`@donaswap/masterchef-v3/deployments/${networkName}.json`)

  const donaswapV3Factory_address = v3DeployedContracts.DonaswapV3Factory

  const DonaswapV3LmPoolDeployer = await ethers.getContractFactory('DonaswapV3LmPoolDeployer')
  const donaswapV3LmPoolDeployer = await DonaswapV3LmPoolDeployer.deploy(mcV3DeployedContracts.MasterChefV3)

  console.log('donaswapV3LmPoolDeployer deployed to:', donaswapV3LmPoolDeployer.address)

  const donaswapV3Factory = new ethers.Contract(donaswapV3Factory_address, abi, owner)

  await donaswapV3Factory.setLmPoolDeployer(donaswapV3LmPoolDeployer.address)

  const contracts = {
    DonaswapV3LmPoolDeployer: donaswapV3LmPoolDeployer.address,
  }
  fs.writeFileSync(`./deployments/${networkName}.json`, JSON.stringify(contracts, null, 2))
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
